﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HexGrid : MonoBehaviour {

    public int width = 6;
    public int height = 5;
    private int sizeWidth;
    private int sizeHeight;
    private float sizeY;
    private float sizeX;
    public float cellSizeX=0.768f;

    public float cellSizeY = 0.655f;
    public float cellOffset = 0.4f;
    private Object[] hexagonsPrefabs;
    private List<HexContainer> cellPrefabs = new List<HexContainer>();
    public HexContainer startHex;
    public List<List<HexContainer>> hexagons = new List<List<HexContainer>>();
    public List<List<HexContainer>> hexagonsState = new List<List<HexContainer>>();
    public int[] indexes;
    private int randomY;
    private int randomX;

 
   
    // Use this for initialization
    void Start()
    {
        sizeY = height * (cellSizeY + cellOffset) - (cellSizeY + cellOffset);
        sizeX = width * (cellSizeX + cellOffset) - (cellSizeX + cellOffset);
        sizeWidth = width - 1;
        sizeHeight = height - 1;

        GameManager gameManager = GameObject.FindObjectOfType<GameManager>().GetComponent<GameManager>();
        hexagonsState = gameManager.hexagons;
       
        if (hexagonsState.Count != 0)
        {
            for(int y = 0; y < hexagonsState.Count; y++)
            {
                hexagons.Add(new List<HexContainer>());
                for (int x = 0; x < hexagonsState[y].Count; x++)
                {
                    CreateCellFromExist(y, x);
                }
            }
            StartCoroutine(lookForPropablyHexagons(gameManager.activeHex[0], gameManager.activeHex[1], 0f));
        }
        else
        {
         
            randomY = Random.Range(0, sizeHeight);
            int count = sizeWidth;
            if (randomY % 2 == 1)
            {
                count--;
            }
            randomX = Random.Range(0, count);

            hexagonsPrefabs = Resources.LoadAll("Hexagons");
            foreach (GameObject hex in hexagonsPrefabs)
            {
                cellPrefabs.Add(hex.GetComponent<HexContainer>());
            }

            for (int y = 0; y < height; y++)
            {
                hexagons.Add(new List<HexContainer>());
                hexagonsState.Add(new List<HexContainer>());
                int hexagonsInRow;
                hexagonsInRow = width;
                if (y % 2 == 1)
                {

                    hexagonsInRow = hexagonsInRow - 1;
                }
                for (int x = 0; x < hexagonsInRow; x++)
                {
                    if (y == randomY && x == randomX)
                    {
                        CreateCell(y, x, 0, true);

                    }
                    else
                    {
                        int[] arr = Enumerable.Range(0, cellPrefabs.Count).OrderBy(c => Random.Range(0, 1000)).ToArray();
                        int i = Random.Range(0, cellPrefabs.Count);
                        CreateCell(y, x, arr[i]);
                    }
                }
            }
            StartCoroutine(lookForPropablyHexagons(randomY, randomX, 6f));
        }
        
    }
    void CreateCell(int y, int x, int i, bool start = false)
    {
        //-2.01-1.41
        Vector3 position;
        position.x = x * (cellSizeX + cellOffset) + (y % 2 * ((cellSizeX + cellOffset) / 2)) - sizeX / 2;
        position.z = 0f;
        position.y = (y * (cellSizeY + cellOffset)) - sizeY / 2;
        if (start)
        {
            hexagons[y].Add(Instantiate(startHex));
            hexagonsState[y].Add(startHex);
        }
        else
        {
            hexagons[y].Add(Instantiate(cellPrefabs[i]));
            hexagonsState[y].Add(cellPrefabs[i]);
        }
        hexagons[y][x].hex.positionY = y;
        hexagons[y][x].hex.positionX = x;

        hexagons[y][x].transform.SetParent(transform, false);
        hexagons[y][x].transform.localPosition = position;

    }
    void CreateCellFromExist(int y, int x)
    {
      
        //-2.01-1.41
        Vector3 position;
        position.x = x * (cellSizeX + cellOffset) + (y % 2 * ((cellSizeX + cellOffset) / 2)) - sizeX / 2;
        position.z = 0f;
        position.y = (y * (cellSizeY + cellOffset)) - sizeY / 2;
        hexagons[y].Add(null);
        hexagons[y][x]=Instantiate(hexagonsState[y][x]);
        hexagons[y][x].hex.positionY = y;
        hexagons[y][x].hex.positionX = x;
        hexagons[y][x].transform.SetParent(transform, false);
        hexagons[y][x].transform.localPosition = position;

    }
    private IEnumerator lookForPropablyHexagons(int activeY, int activeX, float time=0)
    {
        yield return new WaitForSeconds(time);
        List<int[]> propablyIndexes = new List<int[]>();
        for (var y = activeY - 1; y < activeY + 2; y++)
        {
            if (y>=0 && y < height)
            {
                int lineSize = y % 2 == 0 ? 6 : 5;
                int[] range = (y == activeY) ? new int[] { activeX-1, activeX +1} : new int[] { activeX + (lineSize - 6) , activeX - (5 - lineSize) };
                for (var x = range[0]; x <= range[1]; x++)
                {
                    if((y != activeY || x != activeX) && (x >= 0 && x < lineSize)  )
                    {
                        propablyIndexes.Add(new int[] { y,x });
                    }
                }

            }
        }
        resetHexagons();
        setPropablyHexagons(propablyIndexes);
        hexagons[activeY][activeX].hex.setIsActive();
        GameObject.FindObjectOfType<GameManager>().GetComponent<GameManager>().activeHex[0] = activeY;
        GameObject.FindObjectOfType<GameManager>().GetComponent<GameManager>().activeHex[1] = activeX;
    }
   
    void resetHexagons()
    {
        foreach (List<HexContainer> hexagonsLine in hexagons)
        {
            foreach (HexContainer hexagon in hexagonsLine)
            {
                hexagon.hex.setIdle();
              
                //hexagon.hex.setIsActive();
            }
        }

    }
    void setPropablyHexagons(List<int[]> listPropablyHexagons)
    {
       

        foreach (int[] indexes in listPropablyHexagons)
        {
            int y = indexes[0];
            int x = indexes[1];
            hexagons[y][x].hex.setIsPropably();
        }
    }
    public void setActiveHexagon(int y, int x)
    {

        StartCoroutine(lookForPropablyHexagons(y, x));

        //hexagons[y][x].hex.setIsActive();
    }
    // Update is called once per frame
    void Update () {

        //transform.Rotate(p * Time.deltaTime);
        //if (!_isFlipped)
        //{
            float tiltAroundY = Input.GetAxis("Mouse Y") * 30f;
            float tiltAroundX = Input.GetAxis("Mouse X") * 30f;
            Quaternion target = Quaternion.Euler(tiltAroundY, tiltAroundX, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * Config_vars.tiltSpeed);
       // }else
       // {
           // Quaternion target = Quaternion.Euler(180,0, 0);
           // transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 5);
       // }
        
       
    }
}
