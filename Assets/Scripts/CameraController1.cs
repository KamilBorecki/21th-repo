﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController1 : MonoBehaviour {
    public float CameraShakeStartRange = 1f;
    float randY, randX, randZ;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        randY = Random.Range(CameraShakeStartRange * (-1f), CameraShakeStartRange);
        randX = Random.Range(CameraShakeStartRange * (-1f), CameraShakeStartRange);
        randZ = Random.Range(CameraShakeStartRange * (-1f), CameraShakeStartRange);
        Quaternion target = Quaternion.Euler(randY, randX, randZ);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 0.5f);
    }
}
