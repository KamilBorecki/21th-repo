﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Animator cover = GameObject.Find("coverContainer").GetComponent<Animator>();
        if (cover != null)
        {
            cover.SetTrigger("fadeOut");
        }
       
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
