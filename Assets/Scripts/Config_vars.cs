﻿
[System.Serializable]
public static class Config_vars {

    public static float hoverPosZ=-0.8f;
    public static float activePosZ=-0.4f;
    public static float hoverActivePosZ = -0.2f;
    public static float hoverSpeed = 4f;
    public static float tiltSpeed = 0.5f;
    public static int gameStep = 1;

    public static float inactiveTransparency = 0.3f;
    public static float activeTransparency = 1f;
    public static float activeHoverTransparency = 0.2f;
    public static float propablyTransparency = 0.7f;
    public static float propablyHoverTransparency =0.9f;

    public static float cellVolume = 0.2f;

    


}
