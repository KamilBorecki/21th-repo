﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {

    GameManager gameManager;

    public AudioClip[] musicBackgrounds = new AudioClip[4];
    private int gameStep;
    public static MusicController instance = null;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        gameManager = GameObject.FindObjectOfType<GameManager>().GetComponent<GameManager>();
    }
    void Start () {
        AudioSource audio = GetComponent<AudioSource>();
        gameStep = gameManager.gameStep;
        Config_vars.gameStep = 2;
        audio.clip = musicBackgrounds[gameStep];
        audio.Play();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
