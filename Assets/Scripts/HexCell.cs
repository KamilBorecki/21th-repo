﻿using System.Collections;
using UnityEngine;

public class HexCell : MonoBehaviour {
    public int scene;
    public bool isActive = false;
    public bool isPropably = false;
    public int positionY;
    public int positionX;
    public Texture[] stepsTextures = new Texture[4]; 
    public Plane[] planes = new Plane[4];
    public AudioClip[] audioClips= new AudioClip[4];
    private Vector3 _actualPos;
    private bool _mouseOver = false;
    private Color _oldColor;
    private MeshRenderer _render;
    private int _gameStep;
    private bool _isClicked = false;
    private bool _isMoving = false;
    private AudioSource _audio;
    Animator animator;
    GameManager gameManager;


    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>().GetComponent<GameManager>();
        _audio = GetComponent<AudioSource>();
        _audio.clip = audioClips[_gameStep];
        _audio.volume = Config_vars.cellVolume;
        _gameStep = gameManager.gameStep;
        _actualPos = transform.position;
        _render = GetComponent<MeshRenderer>();
        _render.material.mainTexture = stepsTextures[_gameStep];
    }
  
    void Update(){}
    public void setIsActive()
    {
        isActive = true;
        isPropably = false;
        animator.SetInteger("state", 2);
    }
    public void setIsPropably()
    {
        isActive = false;
        isPropably = true;
        animator.SetInteger("state", 1);
    }
    public void setIdle()
    {
        isActive = isPropably = false;
        animator.SetInteger("state", 0);
    }
    public bool getIsActive()
    {
       return isActive;
    }
    public bool getIsPropably()
    {
        return isPropably;
    }
    void OnMouseDown()
    {
        HexGrid grid = GameObject.FindObjectOfType<HexGrid>().GetComponent<HexGrid>();
       
        animator.SetTrigger("click");
        if (isPropably)
        {
            _audio.Play();
            grid.setActiveHexagon(positionY, positionX);
        }
        else if (isActive)
        {
            gameManager.loadScene(scene);
        }
    }
    void OnMouseEnter()
    {
        _mouseOver = true;

        animator.SetBool("hover", true);
    }

    void OnMouseExit()
    {
        _mouseOver = false;
        animator.SetBool("hover", false);

    }

}
