﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager :MonoBehaviour
{

    public bool isPersistant;
    public int gameStep = 0;
    public List<List<HexContainer>> hexagons = new List<List<HexContainer>>();
    public List<int> activeHex = new List<int>();
    Animator cover;
    public static GameManager instance = null;
    void Awake()
    {
        activeHex.Add(0);
        activeHex.Add(1);

        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
    public void loadScene(int sceneIndex)
    {
       
       cover = GameObject.Find("coverContainer").GetComponent<Animator>();
        cover.SetTrigger("fadeIn");
        if (sceneIndex > 0)
        {
            hexagons = GameObject.FindObjectOfType<HexGrid>().GetComponent<HexGrid>().hexagonsState;
        }else
        {
        }
      

        StartCoroutine(openScene(sceneIndex, 2));
           
       
    }
  
    private IEnumerator openScene(int sceneIndex, float time = 0)
    {
        yield return new WaitForSeconds(time);
        AsyncOperation AO =  SceneManager.LoadSceneAsync(sceneIndex);
        yield return AO;
        cover.SetTrigger("fadeOut");
    }
    //SceneManager.LoadScene(sceneIndex);
}

