﻿using UnityEngine;
using System.Collections;


public class Loader : MonoBehaviour
{
    public GameObject gameManager;
    public GameObject bckgAudio;

    void Awake()
    {
        if (GameManager.instance == null)
            Instantiate(gameManager);
        if (MusicController.instance == null)
            Instantiate(bckgAudio);
    }
}